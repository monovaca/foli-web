'use strict';

/*
	In this file we define a table of tables to localize app's strings. That table works in conjuntion with i18n filter to provided translated strings all accross application
*/

angular.module('FOLIMobileWeb.localeTranslations', [])
	.value('i18nTable', {
		'es': {
			ADDRESS: 'Dirección'

		},
		'ca': {
			ADDRESS: 'Address'
		}
	});