'use strict';

/* Factories */

angular.module('FOLIMobileWeb.factories', ['ngResource'])
/* Service providing operations for working with the html5 localStorage. */
.factory('StorageService', ['$http', '$log',
	function($http, $log) {
		var storageService = {};

		/* Scope for local storage. */
		var LOCAL = 'localStorage';

		/* Scope for session storage. */
		var SESSION = 'sessionStorage';

		/* Retrieve object from localStorage. */
		var get = function(scope, key) {
			if (scope in window && window[scope] !== null) {
				var value = window[scope][key];
				if (value !== undefined) {
					value = JSON.parse(value);
				}
				return value;
			} else {
				$log.error('The browser does not have ' + scope);
			}
		};

		/* Put object into localStorage. */
		var put = function(scope, key, value) {
			try {
				if (scope in window && window[scope] !== null) {
					window[scope][key] = JSON.stringify(value);
				} else {
					$log.error('The browser does not have ' + scope);
				}
			} catch (error) {
				$log.error(error);
			}
		};

		/* Remove object from localStorage. */
		var remove = function(scope, key) {
			if (scope in window && window[scope] !== null) {
				window[scope].removeItem(key);
			} else {
				$log.error('The browser does not have ' + scope);
			}
		};

		/* Delete object from session storage. */
		storageService.deleteFromSession = function(key) {
			remove(SESSION, key);
		}

		/* Retrieve object from session storage. */
		storageService.getFromSession = function(key) {
			return get(SESSION, key);
		}

		/* Put object into local storage. */
		storageService.putIntoSession = function(key, value) {
			put(SESSION, key, value);
		}

		/* Delete object from local storage. */
		storageService.deleteFromLocal = function(key) {
			remove(LOCAL, key);
		}

		/* Retrieve object from local storage. */
		storageService.getFromLocal = function(key) {
			return get(LOCAL, key);
		}

		/* Put object into localStorage. */
		storageService.putIntoLocal = function(key, value) {
			put(LOCAL, key, value);
		}

		return storageService;
	}
])
.factory('ContentService', ['$http', '$q', '$log', 'LanguageService',
	function($http, $q, $log, LanguageService) {
		var contentService = {};

		contentService.getStudiesPage = function () {
			var currentLanguage = LanguageService.getCurrentLanguage();
			var deferred = $q.defer();

			$http({
				headers: {
					'Content-Type': 'application/json'
				},
				method: 'GET',
				url: 'json/' + currentLanguage + '/page/studies.json'
			}).success(function(response) {
				deferred.resolve(response);
			}).error(function(data, status, headers, config) {
				deferred.reject({
					data: data,
					status: status
				});
			});

			return deferred.promise;
		}

		return contentService;
    }
])
.factory('DegreeService', ['$http', '$q', '$log', 'LanguageService',
	function($http, $q, $log, LanguageService) {
		var degreeService = {};

		degreeService.getDegree = function (id) {
			var currentLanguage = LanguageService.getCurrentLanguage();
			var deferred = $q.defer();

			$http({
				headers: {
					'Content-Type': 'application/json'
				},
				method: 'GET',
				url: 'json/' + currentLanguage + '/universidad/' + id + '.json'
			}).success(function(response) {
				deferred.resolve(response);
			}).error(function(data, status, headers, config) {
				deferred.reject({
					data: data,
					status: status
				});
			});

			return deferred.promise;
		}

		return degreeService;
    }
]);