'use strict';

/* Directives */


angular.module('FOLIMobileWeb.directives', [])
	.directive("scrollTo", ["$window",

		/* This directive uses click event to scroll to the target element */
		function($window) {
			return {
				restrict: "AC",
				compile: function() {

					var document = $window.document;

					function scrollInto(idOrName) {

						// Find element with the give id or name and scroll to the first element it finds.
						if (!idOrName) {
							$window.scrollTo(0, 0);
						}

						// Check if an element can be found with id attribute.
						var el = document.getElementById(idOrName);
						if (!el) {
							// Check if an element can be found with name attribute if there is no such id.
							el = document.getElementsByName(idOrName);

							if (el && el.length) {
								el = el[0];
							} else {
								el = null;
							}
						}

						// If an element is found, scroll to the element.
						if (el) {
							el.scrollIntoView();
						}

						// Otherwise, ignore.
					}

					return function(scope, element, attr) {
						element.bind("click", function(event) {
							scrollInto(attr.scrollTo);
						});
					};
				}
			};
		}
	])

/*
	This directive makes data validation occur after input loses focus
*/
.directive('blurValidation', [
	function() {
		var FOCUS_CLASS = "ng-focused";
		return {
			restrict: 'A',
			require: 'ngModel',
			link: function(scope, element, attrs, ctrl) {
				ctrl.$focused = false;
				element.bind('focus', function(evt) {
					element.addClass(FOCUS_CLASS);
					scope.$apply(function() {
						ctrl.$focused = true;
						ctrl.$validated = false;
					});
				}).bind('blur', function(evt) {
					element.removeClass(FOCUS_CLASS);
					scope.$apply(function() {
						ctrl.$focused = false;

						if (0 < attrs.blurValidation.length) {
							scope[attrs.blurValidation](element, attrs, ctrl);
						}

						ctrl.$validated = true;
					});
				});
			}
		}
	}
])


/*
	This directive makes an element to lose its focus as soon as it gets it
*/
.directive('blurMe', [
	function() {
		return {
			link: function(scope, element, attrs, ctrl) {
				element.bind('focus', function(evt) {
					element.blur();
				});
			}
		}
	}
])

.directive('fbLikeBox', [
	function() {
		return {
			restrict: 'C',
			link: function(scope, element, attributes) {
				return typeof FB !== "undefined" && FB !== null ? FB.XFBML.parse(element.parent()[0]) : void 0;
			}
		}
	}
])

/*
	This directive bounds FastClick with provided element. FastClick removes mobile devices' 300ms delay when user makes a touch or a click and thus getting a much faster response
*/
.directive('fastclick', [
	function() {
		return {
			link: function(scope, element, attrs, ctrl) {
				// Apply FastClick in every device except Android, as it produces ghost clicks
				if (!FastClick.prototype.deviceIsAndroid) {
					FastClick.attach(element.get(0));
				}
			}
		}
	}
]);
