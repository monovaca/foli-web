'use strict';

/* Filters */

angular.module('FOLIMobileWeb.filters', ['FOLIMobileWeb.localeTranslations'])
	/* This filter returns a localized string, using localization map defined in i18n.js file. Use: { 'STRING_ID' | i18n }. Examples:
			{ 'ROOM' | i18n } --> 'Habitación' (current language spanish)
			{ 'ROOM' | i18n } --> 'Room' (current language english)
	*/
	.filter('i18n', ['i18nTable', '$location', '$log', 'LanguageService',
		function(localesTable, $location, $log, LanguageService) {
			return function(key) {
				return LanguageService.getCurrentLangTranslation(key);
			};
		}
	]);