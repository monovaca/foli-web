'use strict';


// Declare app level module which depends on filters, and services
angular.module('FOLIMobileWeb', ['FOLIMobileWeb.filters', 'FOLIMobileWeb.services', 'FOLIMobileWeb.factories', 'FOLIMobileWeb.directives',
	'FOLIMobileWeb.controllers', 'FOLIMobileWeb.localeTranslations', 'ezfb'
])
	.config(['$routeProvider', '$locationProvider', '$httpProvider', '$FBProvider',
		function($routeProvider, $locationProvider, $httpProvider, $FBProvider) {
			$routeProvider
				.when('/:language/home', {
					templateUrl: 'partials/home.html',
					controller: 'HomeController'
				})
				.when('/es/estudios', {
					templateUrl: 'partials/studies.html',
					controller: 'StudiesController'
				})
				.when('/es/contacta', {
					templateUrl: 'partials/contact.html',
					controller: 'ContactController'
				})
				.when('/es/universidad/:degree', {
					templateUrl: 'partials/degree.html',
					controller: 'DegreeController'
				})
				.otherwise({
					redirectTo: '/es/home'
				});

			$httpProvider.defaults.headers.common['Accept'] = 'application/json;odata=verbose,charset=utf-8';
			$httpProvider.defaults.headers.common['Content-Type'] = 'application/json;charset=utf-8';
			$FBProvider.setInitParams({
    			appId: ''
  			});
		}
	])
	.run(function($rootScope, $route) {
		/* Listener to reload pages when change language event is launched. */
		$rootScope.$on('changeLanguageEvent', function(event, newLanguageId) {
			$route.reload();
		});
	});