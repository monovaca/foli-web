'use strict';

/* Services */

angular.module('FOLIMobileWeb.services', ['FOLIMobileWeb.localeTranslations'])
	.service('LanguageService', ['i18nTable', '$location', '$log',
		function(localesTable, $location, $log) {

			// Returns app's current language
			this.getCurrentLanguage = function() {
				// path() always starts with a /, so we remove it
				var url = $location.path().substr(1);

				var language = url.split('/')[0];

				// Check if language exist in localesTable, otherwise we use the default language
				if (!(language in localesTable)) {
					language = 'es';
				}

				return language;
			};

			/* Returns key's translation in provided language */
			this.getTranslation = function(key, language) {
				// Check if language exists in localesTable, otherwise we use the default language
				if (!localesTable.hasOwnProperty(language)) {
					language = 'es';
				}

				var localizedText = localesTable[language][key];

				// If localizedText is not defined we warn about it
				if (!localizedText) {
					$log.warn('Label ' + key + ' does not exist');
				}

				return localizedText;
			};

			/* Returns key's translation in provided language */
			var langService = this;
			this.getCurrentLangTranslation = function(key) {
				return langService.getTranslation(key, langService.getCurrentLanguage());
			};
		}
	])
	/*
		This service manages current language when user request to go to another page, that is, takes care of inserting current language in target page's url.
		This service must be used to go to a page instead of directly changing path() value
	*/
	.service('NavigationService', ['i18nTable', '$location', '$log', '$timeout', 'LanguageService', 'StorageService', 
		function(localesTable, $location, $log, $timeout, LanguageService, StorageService) {
			this.gotoPage = function(target) {
				$location.path(LanguageService.getCurrentLanguage() + target);
			}
		}
	]);